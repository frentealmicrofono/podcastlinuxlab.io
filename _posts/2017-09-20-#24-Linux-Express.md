---
title: "#24 Linux Express"
date: 2017-09-20
category: [linuxexpress]
author: juan
featimg: 2017/24LinuxExpress.png
podcast:
  audio: https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/%2324%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express,]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2017/24LinuxExpress.png)  
Hoy empieza el otoño y nos metemos en faena.Muchas cosas que contar.  

<audio controls>
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/%2324%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
</audio>

Repasamos lo acontecido en estas semanas:

+ [Episodio #34 Directo Maratón Linuxero](http://avpodcast.net/podcastlinux/maraton).
+ Próximo episodio Formatos Libres.
+ Finalistas [Premios Asociación Podcast](http://premios.asociacionpodcast.es/2017_finalistas/).
+ Nos mudamos a Gitlab:  <https://podcastlinux.gitlab.io>
+ Migración a GNU/Linux de un pc de mi colegio.
+ Idea: Un curso sobre crear tu propio podcast.

Las imágenes utilizadas son propiedad de [Freepik.es](http://www.freepik.es/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://feedpress.me/podcastlinux>
+ Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>
